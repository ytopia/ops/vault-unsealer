module gitlab.com/ytopia/ops/vault-unsealer

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/awnumar/memguard v0.22.2
)
