# Allow go version to be set at build
ARG GO_VERSION=1.15

FROM golang:${GO_VERSION}-alpine as build

COPY . /go/src/gitlab.com/ytopia/ops/vault-unsealer

WORKDIR /go/src/gitlab.com/ytopia/ops/vault-unsealer
RUN go build -o=/go/src/gitlab.com/ytopia/ops/vault-unsealer/vault-unsealer cmd/unsealer/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=build /go/src/gitlab.com/ytopia/ops/vault-unsealer/vault-unsealer /usr/local/bin/
RUN mkdir -p /secrets/unseal
WORKDIR /app
CMD ["vault-unsealer"]
